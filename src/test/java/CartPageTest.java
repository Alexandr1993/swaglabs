import data.DataProviderClass;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.CartPage;
import page.ProductsPage;

import static constants.Constants.COUNT;
import static page.CartPage.*;
import static page.ProductsPage.*;
import static page.CheckoutStepOnePage.*;

@Listeners(ListenerTest.class)
public class CartPageTest extends BaseTest{

    @Test(description = "Проверка добавленного товара в корзину",
            dataProviderClass = DataProviderClass.class, dataProvider = "index")
    public void checkAddToCart(int index) {
        ProductsPage productsPage = new ProductsPage(USER, PASSWORD);
        CartPage cartPage = new CartPage();
        Assert.assertEquals(
                productsPage.descriptionProducts(productsPage, getArrNameProducts().get(index),
                                getArrPriceProducts().get(index), getArrDescriptionProducts().get(index),
                        getArrPriceProducts().get(index))
                        .clickElement(getArrButtonAddToCart().get(index))
                        .clickElement(getCart()).getMapPageResults(),

                cartPage.descriptionProducts(cartPage, getNameCartProducts().get(0),
                                getPriceCartProducts().get(0), getDescriptionCartProducts().get(0),
                                getPriceCartProducts().get(0)).clickElement(getBtnCartRemove().get(0))
                        .clickElement(getBtnCartContinueShopping()).getMapPageResults());
    }

    @Test(description = "Проверка количества добвленного товара на страницу корзины")
    public void checkCountProductsCart() {
        Assert.assertEquals(
                new ProductsPage(USER, PASSWORD)
                        .clickAllButton()
                        .clickCart().getCountProductPage(getNameCartProducts()),
                COUNT.getNumber());
    }

    @Test(description = "Проверка удаление товаров из корзины")
    public void checkRemoveProductsToCart() {
        new ProductsPage(USER, PASSWORD)
                .clickAllButton()
                .clickCart()
                .clickAllButtonRemove()
                .checkExists(getBtnCartRemove().get(0), false);
    }



}
