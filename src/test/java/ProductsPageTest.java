import data.DataProviderClass;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.ItemProductsPage;
import page.ProductsPage;


import static constants.Constants.*;
import static page.ItemProductsPage.*;
import static page.ProductsPage.*;

@Listeners(ListenerTest.class)
public class ProductsPageTest extends BaseTest {

    @Test(description = "Проверка количества товаров на странице")
    public void checkCountProductPage() {
        Assert.assertEquals(
                new ProductsPage(USER, PASSWORD)
                        .getCountProductPage(getImageProduct()), COUNT.getNumber());
    }

    @Test(description = "Проверка количества товаров в корзине")
    public void checkCountProductInCart() {
        Assert.assertEquals(
                new ProductsPage(USER, PASSWORD)
                        .clickAllButton()
                        .getTextElement(getCountProductInCart()),
                Integer.toString(getArrButtonAddToCart().size()));
    }

    @Test(description = "Проверка нажатия кнопки Add to Cart и Remove",
            dataProviderClass = DataProviderClass.class, dataProvider = "index")
    public void checkButton(int indexButton) {
        new ProductsPage(USER, PASSWORD)
                .clickElement(getArrButtonAddToCart().get(indexButton))
                .checkButtonText(NAME_BUTTON_REMOVE.getValue(), indexButton)
                .checkTextElement(getCountProductInCart(), "1")
                .clickElement(getArrButtonAddToCart().get(indexButton))
                .checkButtonText(NAME_BUTTON_ADD.getValue(), indexButton)
                .checkExists(getCountProductInCart(), false);
    }

    @Test(description = "Проверка прокрутки")
    public void checkScroll() {
        new ProductsPage(USER, PASSWORD)
                .scrollToElement(getTitleYear());
    }

    @Test(description = "Проверка отображения изображений товаров на главной странице",
            dataProviderClass = DataProviderClass.class, dataProvider = "index")
    public void checkLoadImage(int indexImage) {
        new ProductsPage(USER, PASSWORD)
                .checkLoadImage(getImageProduct().get(indexImage));
    }

    @Test(description = "Проверка соответвия товара на главной странице и в описании",
            dataProviderClass = DataProviderClass.class, dataProvider = "index")
    public void checkDescriptionProducts(int index) {

        ProductsPage productsPage = new ProductsPage(USER, PASSWORD);
        ItemProductsPage itemProductsPage = new ItemProductsPage();
        Assert.assertEquals(
            productsPage.descriptionProducts(productsPage, getArrNameProducts().get(index),
                    getArrPriceProducts().get(index), getArrDescriptionProducts().get(index),
                    getImageProduct().get(index))
                    .clickElement(getArrNameProducts().get(index)).getMapPageResults(),

            itemProductsPage.descriptionProducts(itemProductsPage, getNameProducts(),
                    getPriceProducts(), getDescriptionProducts(),
                    getImageProducts())
                    .clickElement(getBtnBuck()).getMapPageResults());
    }


    @Test(description = "Проверка сортировки товара")
    public void checkSort() {
        new ProductsPage(USER, PASSWORD)
                .clickElement(getReverseSortingName())
                .checkSort(getArrNameProducts(), true)
                .clickElement(getSortingName())
                .checkSort(getArrNameProducts(), false);
    }

    @Test(description = "Проверка сортировки товара по цене")
    public void checkSortPrice() {

        new ProductsPage(USER, PASSWORD)
                .clickElement(getSortingPriceAsc())
                .checkSort(getArrPriceProducts(), false)
                .clickElement(getSortingPriceDesc())
                .checkSort(getArrPriceProducts(), true);
    }


}
