import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import data.DataProviderClass;
import org.testng.annotations.*;

public class BaseTest {
    protected static final String USER = "standard_user"; ;
    protected static final String PASSWORD = "secret_sauce";
    private void setup() {
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
    }

    @BeforeSuite
    public void init() {
        setup();
    }

    @AfterSuite
    public void tearDown() {
        Selenide.closeWebDriver();
    }


    @AfterMethod
    public void clear() {
        Selenide.clearBrowserCookies();
        Selenide.clearBrowserLocalStorage();
    }

}
