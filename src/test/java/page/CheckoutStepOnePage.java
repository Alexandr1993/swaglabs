package page;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class CheckoutStepOnePage extends Page {

    private static final SelenideElement firstName = $(By.name("firstName"));
    private static final SelenideElement lastName = $(By.name("lastName"));
    private static final SelenideElement postalCode = $(By.name("postalCode"));
    private static final SelenideElement buttonContinue = $(By.name("continue"));
    private static final SelenideElement buttonCancel = $(By.name("cancel"));
    private static final SelenideElement errorMessage = $x("//div[@class=\"error-message-container error\"]");
    private static final SelenideElement buttonError = $x("//button[@class=\"error-button\"]");
    private static final SelenideElement checkoutName = $x("//div[@class=\"inventory_item_name\"]");
    private static final SelenideElement checkoutDescription = $x("//div[@class=\"inventory_item_desc\"]");
    private static final SelenideElement checkoutPrice = $x("//div[@class=\"inventory_item_price\"]");
    private static final SelenideElement itemTotal = $(".summary_subtotal_label");
    private static final SelenideElement buttonFinish = $(By.id("finish"));

    public static SelenideElement getButtonFinish() {
        return buttonFinish;
    }

    public static SelenideElement getCheckoutDescription() {
        return checkoutDescription;
    }

    public static SelenideElement getCheckoutName() {
        return checkoutName;
    }

    public static SelenideElement getCheckoutPrice() {
        return checkoutPrice;
    }

    public static SelenideElement getButtonCancel() {
        return buttonCancel;
    }

    public static SelenideElement getButtonContinue() {
        return buttonContinue;
    }

    public static SelenideElement getFirstName() {
        return firstName;
    }

    public static SelenideElement getLastName() {
        return lastName;
    }

    public static SelenideElement getPostalCode() {
        return postalCode;
    }

    public static SelenideElement getErrorMessage() {
        return errorMessage;
    }

    public static SelenideElement getButtonError() {
        return buttonError;
    }

    /**
     *
     * @return Возвращает цену на странице покупки товара
     */
    public static String textItemPrice() {
        String[] array = itemTotal.getText().split(" ");
        return array[array.length-1];
    }
}
