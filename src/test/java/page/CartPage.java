package page;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;
import static page.ProductsPage.getArrButtonAddToCart;
import static page.ProductsPage.getCart;

public class CartPage extends Page {
     public CartPage(String user, String password, int indexProducts) {
         ProductsPage productsPage = new ProductsPage(user, password);
         productsPage
                 .clickElement(getArrButtonAddToCart().get(indexProducts))
                 .clickElement(getCart());
     }
    public CartPage() {

    }

    private static final ElementsCollection nameCartProducts = $$(".inventory_item_name");
    private static final ElementsCollection descriptionCartProducts = $$(".inventory_item_desc");
    private static final ElementsCollection priceCartProducts = $$(".inventory_item_price");
    private static final ElementsCollection btnCartRemove = $$x("//button[contains(@id, \"remove\")]");
    private static final SelenideElement btnCartContinueShopping = $(By.id("continue-shopping"));
    private static final SelenideElement btnCartCheckout = $(By.id("checkout"));

    public static ElementsCollection getNameCartProducts() {
        return nameCartProducts;
    }

    public static ElementsCollection getPriceCartProducts() {
        return priceCartProducts;
    }

    public static ElementsCollection getDescriptionCartProducts() {
        return descriptionCartProducts;
    }

    public static ElementsCollection getBtnCartRemove() {
        return btnCartRemove;
    }

    public static SelenideElement getBtnCartCheckout() {
        return btnCartCheckout;
    }

    public static SelenideElement getBtnCartContinueShopping() {
        return btnCartContinueShopping;
    }

    /** Метод для нажатия всех кнопок Remove */
    public CartPage clickAllButtonRemove() {
        for (int i = btnCartRemove.size() - 1; i > -1; i--) {
            clickElement(btnCartRemove.get(i));
        }
        return this;
    }

    /** Метод для перехода на страницу оформления покупки */

    public CheckoutStepOnePage clickChekout() {
        btnCartCheckout.click();
        return page(CheckoutStepOnePage.class);
    }

}
