package page;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class MainPage extends Page{

    public MainPage(String url) {
        open(url);
    }

    private static final SelenideElement button = $("#login-button");
    private static final SelenideElement fieldUserName = $("#user-name");
    private static final SelenideElement fieldPassword = $("#password");
    private static final SelenideElement errorMessage = $("h3");
    private static final SelenideElement errorButton = $(".error-button");

    public static SelenideElement getButton() {
        return button;
    }

    public static SelenideElement getFieldUserName() {
        return fieldUserName;
    }

    public static SelenideElement getFieldPassword() {
        return fieldPassword;
    }

    public static SelenideElement getErrorMessage() {
        return errorMessage;
    }

    public static SelenideElement getErrorButton() {
        return errorButton;
    }

    @Override
    public ProductsPage clickElement(SelenideElement element) {
        element.click();
        return page(ProductsPage.class);
    }


}
