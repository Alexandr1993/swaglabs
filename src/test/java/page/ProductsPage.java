package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;

import java.util.*;

import static constants.Constants.*;

import static com.codeborne.selenide.Selenide.*;
import static constants.ConstantsKeysMap.*;
import static constants.ConstantsKeysMap.IMAGE;
import static page.ItemProductsPage.*;
import static page.ItemProductsPage.getImageProducts;
import static page.MainPage.*;


public class ProductsPage extends Page {

    public ProductsPage (String user, String password) {
        openProductsPage(user, password);
    }

    public ProductsPage() {

    }

    private static final ElementsCollection imageProduct = $$x("//a/img");
    private static final ElementsCollection arrButtonAddToCart = $$x("//button[contains(@class, \"btn\")]");
    private static final ElementsCollection arrNameProducts = $$x("//div[@class=\"inventory_item_name\"]");
    private static final ElementsCollection arrPriceProducts = $$x("//div[@class=\"inventory_item_price\"]");
    private static final ElementsCollection arrDescriptionProducts = $$x("//div[@class=\"inventory_item_desc\"]");
    private static final SelenideElement countProductInCart = $x("//span[@class=\"shopping_cart_badge\"]");
    private static final SelenideElement titleYear = $x("//div[@class=\"footer_copy\"]");

    private static final SelenideElement sortingName = $("[value=\"az\"]");
    private static final SelenideElement reverseSortingName = $("[value=\"za\"]");
    private static final SelenideElement sortingPriceDesc= $("[value=\"hilo\"]");
    private static final SelenideElement sortingPriceAsc = $("[value=\"lohi\"]");
    private static final SelenideElement cart = $(".shopping_cart_link");


    /** Метод для нажатия всеx кнопок "Add to Cart" на странице */
    public ProductsPage clickAllButton() {
        for (int i = 0; i < arrButtonAddToCart.size(); i++) {
            clickElement(arrButtonAddToCart.get(i));
        }
        return this;
    }
    @Override
    public ProductsPage checkButtonText(String text, int indexButton) {
        arrButtonAddToCart.get(indexButton).shouldHave(Condition.text(text));
        return this;
    }

    public static SelenideElement getCountProductInCart() {
        return countProductInCart;
    }

    public static ElementsCollection getArrButtonAddToCart() {
        return arrButtonAddToCart;
        
    }

    public static SelenideElement getTitleYear() {
        return titleYear;
    }

    public static ElementsCollection getImageProduct() {
        return imageProduct;
    }

    public static ElementsCollection getArrDescriptionProducts() {
        return arrDescriptionProducts;
    }

    public static ElementsCollection getArrNameProducts() {
        return arrNameProducts;
    }

    public static ElementsCollection getArrPriceProducts() {
        return arrPriceProducts;
    }

    public static SelenideElement getSortingName() {
        return sortingName;
    }

    public static SelenideElement getReverseSortingName() {
        return reverseSortingName;
    }

    public static SelenideElement getSortingPriceAsc() {
        return sortingPriceAsc;
    }

    public static SelenideElement getSortingPriceDesc() {
        return sortingPriceDesc;
    }


    public static SelenideElement getCart() {
        return cart;
    }

    /** Метод для создания массива имен товаров */
    protected Object[] elementsCollectionToArray(ElementsCollection collection) {
        Object[] array = new Object[collection.size()];
        for(int i = 0; i < collection.size(); i++) {
            array[i] = collection.get(i).getText();
        }
        if (array[0].toString().startsWith("$")) {
            for(int i = 0; i < array.length; i++) {
                array[i] = Double.parseDouble(array[i].toString().replace("$", ""));
            }
        }

        return array;
    }

    /** Метод для сортировка массива */
    protected Object[] sorting(Object[] array, boolean reverse) {
        if (reverse) {
            Arrays.sort(array, Collections.reverseOrder());
        }
        else {
            Arrays.sort(array);
        }
        return array;
    }

    /** Метод для проверки сортировки товаров, reverse = true - по алфавиту в обратном порядке
     * reverse = false - в алфавитном порядке*/
    @Override
    public ProductsPage checkSort(ElementsCollection collection, boolean reverse) {
        Assert.assertEquals(elementsCollectionToArray(collection),
                sorting(elementsCollectionToArray(collection), reverse));
        return this;
    }

    /** Метод для перехода в корзину */
    public CartPage clickCart() {
        cart.click();
        return page(CartPage.class);
    }




}
