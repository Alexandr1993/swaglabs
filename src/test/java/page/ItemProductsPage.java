package page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class ItemProductsPage extends Page {

    private static final SelenideElement nameProducts = $x("//div[contains(@class,\"inventory_details_name\")]");
    private static final SelenideElement priceProducts = $x("//div[contains(@class,\"inventory_details_price\")]");
    private static final SelenideElement descriptionProducts = $x("//div[contains(@class,\"inventory_details_desc l\")]");
    private static final SelenideElement imageProducts = $x("//img[contains(@class,\"inventory_details_i\")]");
    private static final SelenideElement btnAddToCart = $x("//button[contains(@class,\"btn btn_primary\")]");
    private static final SelenideElement btnBuck = $x("//button[contains(@class,\"btn btn_sec\")]");

    public static SelenideElement getBtnAddToCart() {
        return btnAddToCart;
    }

    public static SelenideElement getBtnBuck() {
        return btnBuck;
    }

    public static SelenideElement getImageProducts() {
        return imageProducts;
    }

    public static SelenideElement getDescriptionProducts() {
        return descriptionProducts;
    }

    public static SelenideElement getNameProducts() {
        return nameProducts;
    }

    public static SelenideElement getPriceProducts() {
        return priceProducts;
    }

}
