package page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.util.HashMap;
import java.util.Map;

import static constants.Constants.BASE_URL;
import static constants.ConstantsKeysMap.*;
import static constants.ConstantsKeysMap.IMAGE;
import static page.ItemProductsPage.*;
import static page.ItemProductsPage.getImageProducts;
import static page.MainPage.*;

public abstract  class Page {

    private Map<Object, Object> mapPageResults;

    /**
     * Метод для открытия страницы ProductsPage
     * @param user - логин
     * @param password - пароль
     */
    public void openProductsPage (String user, String password) {
        new MainPage(BASE_URL.getValue())
                .textInput(getFieldPassword(), password)
                .textInput(getFieldUserName(), user)
                .clickElement(getButton());
    }

    /** Получение текста элемента */
    public String getTextElement(SelenideElement element) {
        return element.getText();
    }

    /** Клик по элементу */
    public Page clickElement(SelenideElement element) {
        element.click();
        return this;
    }

    /** Ввод текста в поле и проверка, что введено*/
    public Page textInput(SelenideElement element, String text) {
        element.setValue(text).shouldHave(Condition.value(text));
        return this;
    }

    /** Проверка текста кнопки */
    public Page checkButtonText(String text, int indexButton) {
        return this;
    }

    /** Проверка текста элемента */
    public Page checkTextElement(SelenideElement element, String text) {
        element.shouldHave(Condition.text(text));
        return this;
    }

    /** Прокрутка до необходимого элемента */

    public Page scrollToElement(SelenideElement element) {
        element.scrollIntoView(true);
        return this;
    }

    /** Метод для проверки загружено ли изображение */
    public Page checkLoadImage(SelenideElement element) {
        element.shouldHave(Condition.image);
        return this;
    }

    /** Получение атрибута элемента */
    public String getAttr(SelenideElement element, String attribute) {
        return element.getAttribute(attribute);
    }

    /** Проверку существования элемента
     * если exists = true, то проверка существования элемента,
     * если exists = false, то проверка не существования элемента */
    public Page checkExists(SelenideElement element, boolean exists) {
        if (exists) {
            element.shouldHave(Condition.exist);
        }
        else {
            element.shouldHave(Condition.not(Condition.exist));
        }
        return this;
    }

    /** Метод для проверки сортировки товаров */
    public Page  checkSort(ElementsCollection collection, boolean reverse) {
        return this;
    };

    /** Метод для заполнения описания товара в hashMap */
    public Page descriptionProducts (Page page,
                                                    SelenideElement name,
                                                    SelenideElement price,
                                                    SelenideElement description,
                                                    SelenideElement image) {
        mapPageResults = new HashMap<>();
        {
            mapPageResults.put(NAME, page
                    .getTextElement(name));
            mapPageResults.put(PRICE, page
                    .getTextElement(price));
            mapPageResults.put(DESCRIPTION, page
                    .getTextElement(description));
            mapPageResults.put(IMAGE, page
                    .getAttr(image, "src"));
        }

        return this;
    }

    public Map<Object, Object> getMapPageResults() {
        return mapPageResults;
    }

    /** Метод для получения количества товаров на странице */
    public int getCountProductPage(ElementsCollection elementCollection) {
        return elementCollection.size();
    }



}


