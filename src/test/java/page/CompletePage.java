package page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static page.CartPage.getBtnCartCheckout;
import static page.CheckoutStepOnePage.*;
import static page.CheckoutStepOnePage.getButtonContinue;

public class CompletePage extends Page {

    public CompletePage(String user, String password) {

        new CartPage(user, password, 0)
                .clickElement(getBtnCartCheckout())
                .textInput(getFirstName(), "Александр")
                .textInput(getLastName(), "Александров")
                .textInput(getPostalCode(), "4444")
                .clickElement(getButtonContinue())
                .clickElement(CheckoutStepOnePage.getButtonFinish());

    }

    private static SelenideElement textComplete = $(".complete-header");
    private static SelenideElement imageOk = $("[alt=\"Pony Express\"]");
    private static SelenideElement buttonBuck = $("#back-to-products");

    public static SelenideElement getImageOk() {
        return imageOk;
    }

    public static SelenideElement getButtonBuck() {
        return buttonBuck;
    }

    public static SelenideElement getTextComplete() {
        return textComplete;
    }
}
