package constants;

public enum Constants {

    BASE_URL("https://www.saucedemo.com/"),
    EXPECTED_URL("https://www.saucedemo.com/inventory.html"),
    MESSAGE_ERROR_AUTHORIZATION("Epic sadface: Username and password do not match any user in this service"),
    MESSAGE_LOCKED_AUTHORIZATION("Epic sadface: Sorry, this user has been locked out."),
    NAME_BUTTON_REMOVE("Remove"),
    NAME_BUTTON_ADD("Add to cart"),
    MES_ERR_FIRST_NAME("Error: First Name is required"),
    MES_ERR_LAST_NAME("Error: Last Name is required"),
    MES_ERR_CODE("Error: Postal Code is required"),
    MESSAGE_COMPLETE("Thank you for your order!"),
    COUNT(6);

    private String value;
    private int number;

    Constants(String value) {
        this.value = value;
    }

    Constants(int number) {
        this.number = number;
    }

    public String getValue() {
        return value;
    }

    public int getNumber() {
        return number;
    }

}
