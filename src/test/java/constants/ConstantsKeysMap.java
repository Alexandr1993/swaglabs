package constants;

public enum ConstantsKeysMap {

    NAME,
    PRICE,
    DESCRIPTION,
    IMAGE;
}
