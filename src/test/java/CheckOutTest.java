import com.codeborne.selenide.WebDriverRunner;
import data.DataProviderClass;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.CartPage;
import page.CheckoutStepOnePage;
import page.CompletePage;

import static constants.Constants.*;
import static page.CartPage.*;
import static page.CheckoutStepOnePage.*;
import static page.CompletePage.*;


@Listeners(ListenerTest.class)
public class CheckOutTest extends BaseTest {


    @Test(description = "Проверка ввода данных при офoрмлении покупки")
    public void checkDateCheckout() {
        new CartPage(USER, PASSWORD, 0)
                .clickElement(getBtnCartCheckout())
                .textInput(getFirstName(), "Александр")
                .textInput(getLastName(), "Александров")
                .textInput(getPostalCode(), "4444")
                .clickElement(getButtonContinue());
    }

    @Test(description = "Проверка сообщения ошибки, при незаполненных данных")
    public void checkErrorMessage() {
        new CartPage(USER, PASSWORD, 0)
                .clickElement(getBtnCartCheckout())
                .clickElement(getButtonContinue())
                .checkTextElement(getErrorMessage(), MES_ERR_FIRST_NAME.getValue())
                .clickElement(getButtonError()).checkExists(getErrorMessage(), false)
                .textInput(getFirstName(), "Александр")
                .clickElement(getButtonContinue())
                .checkTextElement(getErrorMessage(), MES_ERR_LAST_NAME.getValue())
                .clickElement(getButtonError()).checkExists(getErrorMessage(), false)
                .textInput(getLastName(), "Александров")
                .clickElement(getButtonContinue())
                .checkTextElement(getErrorMessage(), MES_ERR_CODE.getValue())
                .clickElement(getButtonError()).checkExists(getErrorMessage(), false);

    }

    @Test(description = "Проверка покупаемого товара на соответствие в корзине",
            dataProviderClass = DataProviderClass.class, dataProvider = "index")
    public void checkCheckoutOnCart(int index) {
        CartPage cartPage = new CartPage(USER, PASSWORD, index);
        CheckoutStepOnePage checkStep = new CheckoutStepOnePage();
        Assert.assertEquals(
                cartPage.descriptionProducts(cartPage,
                                getNameCartProducts().get(0), getPriceCartProducts().get(0),
                                getDescriptionCartProducts().get(0), getPriceCartProducts().get(0))
                        .clickElement(getBtnCartCheckout())
                        .textInput(getFirstName(), "Александр")
                        .textInput(getLastName(), "Александров")
                        .textInput(getPostalCode(), "4444")
                        .clickElement(getButtonContinue())
                        .getMapPageResults(),

                checkStep.descriptionProducts(checkStep,
                        getCheckoutName(), getCheckoutPrice(),
                        getCheckoutDescription(), getCheckoutPrice())
                        .getMapPageResults());

    }

    @Test(description = "Проверки цены на товар при покупке",
    dataProviderClass = DataProviderClass.class, dataProvider = "index")
    public void checkPriceOnCheckout(int index) {
        new CartPage(USER, PASSWORD, index)
                .clickElement(getBtnCartCheckout())
                .textInput(getFirstName(), "Александр")
                .textInput(getLastName(), "Александров")
                .textInput(getPostalCode(), "4444")
                .clickElement(getButtonContinue());
        Assert.assertEquals(CheckoutStepOnePage.textItemPrice(), getCheckoutPrice().getText());
    }

    @Test(description = "Проверка успешной покупки")
    public void checkFinish() {
        Assert.assertEquals(new CompletePage(USER, PASSWORD)
                .getTextElement(CompletePage.getTextComplete()), MESSAGE_COMPLETE.getValue());
    }

    @Test(description = "Проверка значка успешной покупки")
    public void checkIconFinish() {
        new CompletePage(USER, PASSWORD)
                .checkExists(getImageOk(), true);
    }

    @Test(description = "Проверка на главную страницу со страницы завершенной покупки")
    public void checkBuckOnMain() {
        new CompletePage(USER, PASSWORD)
                .clickElement(getButtonBuck());
        Assert.assertEquals(WebDriverRunner.url(), EXPECTED_URL.getValue());
    }



}
