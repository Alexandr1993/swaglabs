import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ListenerTest implements ITestListener {
    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("Старт теста " + result.getName() + " !");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Тест " + result.getName() + " успешно завершен!");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("Тест " + result.getName() + " не пройден!!!");
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("Тест " + result.getName() + " пропущен!");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("Тест " + result.getName() + " пройден частично!!!");
    }

    @Override
    public void onTestFailedWithTimeout(ITestResult result) {
        System.out.println("Тест " + result.getName() + " не пройден, время ожидания превышено!!!");
    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("Начало тестирования " + context.getName() + " !");
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println("Завершение тестирования " + context.getName());
    }
}
