import com.codeborne.selenide.WebDriverRunner;
import data.DataProviderClass;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import page.MainPage;

import static constants.Constants.*;
import static page.MainPage.*;

@Listeners(ListenerTest.class)
public class AuthorizationTest extends BaseTest {


    @Test(description = "Проверка авторизации, пароли и имена есть в системе",
            dataProviderClass = DataProviderClass.class, dataProvider = "positivePasswordUser")
    public void checkPositiveAuthorization(String user, String password) {

        new MainPage(BASE_URL.getValue())
                .textInput(getFieldPassword(), password)
                .textInput(getFieldUserName(), user)
                .clickElement(getButton());
        Assert.assertEquals(WebDriverRunner.url(), EXPECTED_URL.getValue());

    }

    @Test(description = "Проверка авторизации, пароли и имена отсутсвуют в системе",
            dataProviderClass = DataProviderClass.class, dataProvider = "negativePasswordUser")
    public void checkNegativeAuthorization(String user, String password) {

        Assert.assertEquals(
                new MainPage(BASE_URL.getValue())
                        .textInput(getFieldPassword(), password)
                        .textInput(getFieldUserName(), user)
                        .clickElement(getButton())
                        .getTextElement(getErrorMessage()),
                        MESSAGE_ERROR_AUTHORIZATION.getValue());

    }

    @Test(description = "Проверка авторизации, пользователь заблокирован",
            dataProviderClass = DataProviderClass.class, dataProvider = "lockedPasswordUser")
    public void checkLockedAuthorization(String user, String password) {

        Assert.assertEquals(
                new MainPage(BASE_URL.getValue())
                        .textInput(getFieldPassword(), password)
                        .textInput(getFieldUserName(), user)
                        .clickElement(getButton())
                        .getTextElement(getErrorMessage()),
                MESSAGE_LOCKED_AUTHORIZATION.getValue());

    }

    @Test(description = "Проверка закрытия сообщения ошибки",
            dataProviderClass = DataProviderClass.class, dataProvider = "negativePasswordUser")
    public void checkCloseMessage(String user, String password) {

        new MainPage(BASE_URL.getValue())
                .textInput(getFieldPassword(), password)
                .textInput(getFieldUserName(), user)
                .clickElement(getButton())
                .checkExists(getErrorMessage(), true)
                .clickElement(getErrorButton())
                .checkExists(getErrorMessage(), false);

    }

}
