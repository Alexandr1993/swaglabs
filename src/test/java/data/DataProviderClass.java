package data;

import org.testng.annotations.DataProvider;
import page.ProductsPage;

import static page.ProductsPage.*;

public class DataProviderClass {
    @DataProvider
    public static Object[][] index() {

        return new Object[][] {
                {0}, {1}, {2}, {3}, {4}, {5}
        };
    }

    @DataProvider
    public static Object[][] positivePasswordUser() {

        return new Object[][] {
                {"standard_user", "secret_sauce"},
                {"problem_user", "secret_sauce"},
                {"performance_glitch_user", "secret_sauce"}
        };
    }

    @DataProvider
    public static Object[][] negativePasswordUser() {

        return new Object[][] {
                {"standard_useru", "secret_sauce"},
                {"standard_user", "secret_sauce2"}
        };
    }

    @DataProvider
    public static Object[][] lockedPasswordUser() {

        return new Object[][] {
                {"locked_out_user", "secret_sauce"}
        };
    }

    @DataProvider
    public static Object[][] s() {

        return new Object[][] {
                {getSortingName(), false},
                {getReverseSortingName(), true}
        };
    }

}
